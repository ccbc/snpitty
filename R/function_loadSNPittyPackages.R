#' @title Loads and installs all the crucial required packages used by SNPitty.
#'
#' @examples
#' \dontrun{
#'  loadSNPittyPackages()
#' }
#' 
#' @author Job van Riet \email{j.vanriet@erasmusmc.nl}
#' @export
loadSNPittyPackages <- function(){
  
  futile.logger::flog.info(paste('loadSNPittyPackages - Loading libraries.'))
  
  # Used packages.
  neededPackages <- c('tools', 'devtools', 'shiny', 'shinydashboard', 'shinyBS', 'plyr', 'rjson', 'knitr', 'htmltools', 'stringr', 'RColorBrewer', 'futile.logger')
  neededBioCPackages <- c('Biobase', 'VariantAnnotation', 'Gviz', 'ggbio')
  neededGitPackages <- c('ramnathv/rCharts', 'rstudio/DT')
  
  # CRAN packages.
  newPackages <- neededPackages[!(neededPackages %in% utils::installed.packages()[,'Package'])]
  if(length(newPackages)) utils::install.packages(newPackages, repos='http://cran.rstudio.com/')
  
  # BioC packages.
  newBioCPackages <- neededBioCPackages[!(neededBioCPackages %in% utils::installed.packages()[,'Package'])]
  if(length(newBioCPackages)){ source('http://bioconductor.org/biocLite.R');  biocLite(newBioCPackages, suppressUpdates = T, suppressAutoUpdate = T, ask = F)}
  
  # GitHub packages.
  newGitPackages <- neededGitPackages[!(gsub('.*/', '',neededGitPackages) %in% utils::installed.packages()[,'Package'])]
  lapply(newGitPackages, devtools::install_github, character.only = TRUE)
  
  # Require all packages.
  invisible(lapply(c(neededBioCPackages, neededPackages, gsub(pattern = '.*/', replacement = '', x = neededGitPackages)), require, character.only = TRUE))
  
}