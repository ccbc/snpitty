# SNPitty: An intuitive web-application for interactive B-allele frequency and copy-number visualization of next generation sequencing data.

![docker](https://img.shields.io/docker/pulls/ccbc/snpitty.svg)  ![license](https://img.shields.io/badge/license-GPL--3-blue.svg)

Exploration and visualization of next-generation sequencing data is crucial for clinical diagnostics. Software allowing simultaneous visualization of multiple regions-of-interest coupled with dynamic heuristic filtering of genetic aberrations is however lacking.

Therefore, we developed the web-application SNPitty that allows interactive visualization and interrogation of Variant Call Format (VCF) files by utilizing B-allele frequencies (BAF) of single nucleotide polymorphisms (SNPs) and single nucleotide variants (SNVs), coverage metrics and copy-numbers analysis results.

SNPitty is best applicable to display variant alleles and allelic imbalances with a focus on loss of heterozygosity (LOH) and copy number variation using genome-wide heterozygous markers and somatic mutations. Moreover, SNPitty is capable of generating predefined reports that summarize and highlight disease-specific targets-of-interest.

Due to dependencies on the latest R and BioConductor versions, it is **highly advised** to use the pre-compiled Docker image which contains an out-of-the-box working SNPitty and all features.

### For additional background information, validation and several diagnostic use-cases, please see the publication: [Link](https://doi.org/10.1016/j.jmoldx.2017.11.011) 

# Table of Contents
1. [Docker Installation](#markdown-header-docker)
3. [Usage](#markdown-header-usage)
4. [Extra](#markdown-header-extra)


# Installation

## Docker

A [Docker image](https://hub.docker.com/r/ccbc/snpitty/) has been made available for SNPitty which eases installation and deployment.

Simply download Docker for [Windows](https://www.docker.com/products/docker-toolbox) or [Linux](https://docs.docker.com/linux/) and pull the SNPitty image:
```bash
docker pull ccbc/snpitty
```

You can now deploy SNPitty using the following command, SNPitty can then be reached using any modern-web browser:
```bash
# You need to specify an unused port which will be used to connect to SNPitty. Port 3382 is also used to host a local exporting server.
docker run -p <yourport>:3383 -p 3382:3382 --net=host ccbc/snpitty

# E.g. to make SNPitty available on http://<ip>:1234/
# docker run -p 1234:3383 -p 3382:3382 --net=host  ccbc/snpitty
```

For additional Docker options, please refer to the [official Docker documentation](https://docs.docker.com/).

### Updating

New versions of SNPitty can easily be retrieved by pulling the latest image and (re)starting the container.
```bash
# Pull latest image.
docker pull ccbc/snpitty

# Stop old container (if needed).

# Start new container with updated SNPitty.
docker run -p <yourport>:3383 -p 3382:3382 ccbc/snpitty
``` 
***

## Usage
Each connection to SNPitty is run on an independent session which allows multiple users to connect simultanously whilst separating data/users to each respective session.

Multiple VCF and GTF files can be uploaded using the respective upload forms but can also be given as GET parameters in the URL in the following manner to allow linking to sessions with pre-loaded data and custom settings:

```{HTML}
For single VCF file with additional region file:
http://<HOST>:<PORT>/?vcfFile=<vcfFileLocation>&regionFile=<regionFileLocation>
```
Example:
```{HTML}
http://127.0.0.1:1234/?vcfFile=192.168.1.20/sampleX/panelXYZ.vcf&regionFile=192.168.1.20/annotation/panelXYZ.txt
```

These files must be network-accessible or mounted to the Docker container running SNPitty. If the files are mounted to the Docker container, **localhost**/sampleX/panelXYZ.vcf can be used.

### Regions
GTF files containing regional coordinates can be used to group and display multiple variants together. 
This allows users to view/filter on various regions-of-interest, similar to viewing/filtering on chromosomes.
For example, if a GTF file containing the chromosomal start/end locations of disease-specific genes is added, it allows users to only filter and display the *EGFR*, *TP53* and *PTEN* genes. Variants in the VCF file are grouped into regions based on genomic overlap.

### Sessions
SNPitty uses temporary sessions, each new connection to SNPitty (browsing to SNPitty) will create a new unique session. This enables data-separation and allows for multiple users to simultaneously make use of SNPitty on the same server. After closing an active connection to SNPitty (closing the browser/tab) **all server-side session data is removed**.

### Settings
Several settings can be altered from the `settings` page within SNPitty. These are only remembered for the current session.
Another method to provide session-specific settings is to provide these in the URL, e.g:
* http://127.0.0.1:1234/?ratioHomoLineThreshold=0.05;0.9&zoomType=T&logReads=T&markVariants=G/A

The following settings can be altered via URL:

* ratioHomoLineThreshold, e.g.: http://127.0.0.1:1234/?ratioHomoLineThreshold=0.1;0.9
* ratioHeteroLineThreshold, e.g.: http://127.0.0.1:1234/?ratioHeteroLineThreshold=0.4;0.6
* filterOnID, e.g.: http://127.0.0.1:1234/?filterOnID=onlyRS
* flipData, e.g.: http://127.0.0.1:1234/?flipData=T
* zoomType, e.g.: http://127.0.0.1:1234/?zoomType=T
* logReads, e.g.: http://127.0.0.1:1234/?logReads=T
* showPattern, e.g.: http://127.0.0.1:1234/?showPattern=T
* showRegionsOnStart, e.g.: http://127.0.0.1:1234/?showRegionsOnStart=T
* markVariants, e.g.: http://127.0.0.1:1234/?markVariants=rs*
* infoDP, e.g.: http://127.0.0.1:1234/?infoDP=100
* infoSB, e.g.: http://127.0.0.1:1234/?infoSB=0.4;0.6

***

## Extra

### Include additional report templates.
To include additional *Rnw* files (used as report templates), a directory with the *Rnw* files can be mounted into the Docker Container using the following command:

```bash
docker run -v <directoryWithRnwFiles>:/usr/local/lib/R/site-library/SNPitty/shiny/reportTemplates/ ccbc/snpitty
```

### Manual Installation (Not advised)

You can also download and install SNPitty separate from the pre-installed docker environment as an R package.

SNPitty can be downloaded directly from the development branch on BitBucket using the following commands in **R**:
```R
# Require/install devtools package if not already installed.
if (!require("devtools")) install.packages("devtools", repos = "http://cran.r-project.org")
# Install SNPitty from BitBucket
devtools::install_bitbucket(repo = "ccbc/snpitty")

# Download all required packages
library(SNPitty)
SNPitty::loadSNPittyPackages()
```

SNPitty houses an internal function (```loadSNPittyPackages()```) to automatically download and install its dependencies.

To enable VCF file merging, **bcftools**, **tabix** and **bgzip** need to be installed in `/usr/bin/`.
Please follow the independent manuals for each tool to do so.

To start SNPitty from R, use the following command:
```R
# For external access, an open network port must be given.
SNPitty::startSNPitty(host='0.0.0.0', port=<yourPort>)

# E.g.
# SNPitty::startSNPitty(host='0.0.0.0', port=1234)
```

#### Automatic reporting
To enable report functionality the following software needs to be installed:

* pdflatex
    *  Linux: sudo apt-get install texlive-latex-base (Or relevant package manager)
    *  Windows: Download [Miktex](http://miktex.org/download)

#### Local Highcharts exporting server
SNPitty uses the HighCharts JS library to create interactive plots. These plots can be exported as SVG/PNG/PDF images, a local exporting server should be running on port 3382. Please see [here for manual instructions](http://www.highcharts.com/docs/export-module/setting-up-the-server). The Docker already has this functionality installed and running in port 3382.

#### Windows

Windows support (without using Docker) is limited as most of the underlying tools are made with Unix-systems in mind.
Testing shows that the main functions such as uploading VCF/GTF and visualization BAF are working. However, the following functions work only sparsely on Windows:

* VCF merging.
* Export function.